//
//  ViewController.swift
//  targetView
//
//  Created by Admin on 11/23/18.
//  Copyright © 2018 s. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentsCount = 7
        drawTargets()
    }
    
    static let centerSquareSize = 20
    static let segmentThickness = 40
    var segmentsCount = 0
    
    func makeSquare(color: UIColor, x: Int, y: Int, w: Int, h: Int) -> UIView {
        let rect = CGRect.init(x: x, y: y, width: w, height: h)
        let square = UIView.init(frame: rect)
        
        square.backgroundColor = color
        return square
    }
    
    func drawTargets() {
        enum Color {
            case blue
            case red
            
            mutating func next() {
                switch self {
                case .blue: self = .red
                case .red: self = .blue
                }
            }
        }
    
        var color: Color = .red
        var currentSize = ViewController.centerSquareSize +
                          segmentsCount * ViewController.segmentThickness

        while currentSize >= ViewController.centerSquareSize {
            let c = (color == .blue ? UIColor.blue : UIColor.red)
            let square = makeSquare(color: c,
                                    x: (Int(view.frame.width) - currentSize) / 2,
                                    y: (Int(view.frame.height) - currentSize) / 2 - ViewController.segmentThickness * segmentsCount / 2 - 50,
                                    w: currentSize,
                                    h: currentSize)
            view.addSubview(square)
            color.next()
            currentSize -= ViewController.segmentThickness
        }
        
        currentSize = ViewController.centerSquareSize +
            segmentsCount * ViewController.segmentThickness
        color = .red

        while currentSize >= ViewController.centerSquareSize {
            let c = (color == .blue ? UIColor.blue : UIColor.red)
            let arcCenter = CGPoint(x: Int(view.frame.width) / 2,
                                    y: Int(view.frame.height) / 2 + ViewController.segmentThickness * segmentsCount / 2 + 50)
            let radius = CGFloat(currentSize / 2)
            let startAngle = CGFloat(0)
            let endAngle = CGFloat(2 * Double.pi)
            let circlePath = UIBezierPath(arcCenter: arcCenter, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
            let shapeLayer = CAShapeLayer()
            
            shapeLayer.path = circlePath.cgPath
            shapeLayer.fillColor = c.cgColor
            shapeLayer.strokeColor = c.cgColor
            shapeLayer.lineWidth = CGFloat(ViewController.segmentThickness)
            view.layer.addSublayer(shapeLayer)
            color.next()
            currentSize -= ViewController.segmentThickness
        }
    }
}




